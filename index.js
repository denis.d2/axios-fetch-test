import axios from "axios"
import fetch from "node-fetch";
import dotenv from "dotenv"

dotenv.config()

const cookie = `console_authtoken=${process.env.COOKIE}`

const sendRequest = async () => {

    try {
        const iamResult1 = await axios("https://affinidi-iam.prod.affinity-project.org/api/v1/projects", {
            headers: {
                cookie,
            }
        })
        const iamResult2 = await fetch("https://affinidi-iam.prod.affinity-project.org/api/v1/projects",{
            headers: {
                cookie,
            }
        }).then(res => res.json())
        const iamResult3 = await axios("https://affinidi-iam.apse1.affinidi.com/api/v1/projects",  {
            headers: {
                cookie,
            }
        })
        const iamResult4 = await fetch("https://affinidi-iam.apse1.affinidi.com/api/v1/projects", {
            headers: {
                cookie,
            }
        }).then(res => res.json())

        console.log(
            {
                affinityProjectOrgAxios: iamResult1.data,
                affinityProjectOrgFetch: iamResult2,
                affinidiComAxios: iamResult3.data,
                affinidiComFetch: iamResult4,
            }
        )

    } catch (error) {
        console.log(error)
    }
}

sendRequest()