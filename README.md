# Reproducible issue for affinidi services

## How to run
```bash
npm i

cp .env.example .env
## login to console.affinidi.com and get your cookie

npm start
```

Result:
```
{
  affinityProjectOrgAxios: '\x1F�\b\x00\x00\x00\x00\x00\x00\x03|ͽn\x02A\f\x04�WA[������LO�(���\x15�\b\bNJ�x�l�4H M73�n�|9}y_��}�·\x1D=������蛿n#a��{�GO�JO�\x10�\x19$Q\x05��KO2�Q�}\\��m�y��\x0B#3\x10��1�H\ryb��p�>e��M�L�\n' +
    '�t\x10Z"\x1C�T@%\x19l�d�\x19�{ʍKC�4�W�\x03�`QSG��\x12�i�Z��WΚ\x0Fi��_�T\x1B�Ě\x07�y�\x05\x00\x00��\x03\x00�\x1Eo�q\x01\x00\x00',
  affinityProjectOrgFetch: { projects: [ [Object], [Object], [Object] ] },
  affinidiComAxios: { projects: [ [Object], [Object], [Object] ] },
  affinidiComFetch: { projects: [ [Object], [Object], [Object] ] }
}
``